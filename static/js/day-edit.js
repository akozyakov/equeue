import * as util from "./util.js";

function saveDayServices() {
    const $list = $('#services-list input:checked');
    const services = $.map($list, util.getID).map(s => Number.parseInt(s));

    util.postJSON(
        '/post/day-edit',
        {services}
    );
}

function initAll() {
    $("#button-save-services").click(saveDayServices);
}

initAll();