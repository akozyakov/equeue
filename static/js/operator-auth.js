import * as util from "./util.js";

const $errorMsg = $('#error-msg');

async function authClick() {
    util.hide($errorMsg);
    try {
        await $.post(
            '/post/operator-auth',
            {
                name: $('#operator-name').val(),
                password: $('#operator-password').val(),
                window: $('#select-operator-window').val(),
                services: $.map($('#services-list input:checked'), util.getID),
            }
        );
        location.assign('/operator-workplace');
    }
    catch(err)  {
        if (err.responseJSON && err.responseJSON.message) {
            $errorMsg.text(err.responseJSON.message);
        } else {
            $errorMsg.text(`${err.status} ${err.statusText}`);
        }
        util.show($errorMsg);
    }
}

function initAll() {
    $('#button-auth').click(authClick);
}

initAll();