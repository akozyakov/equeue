import * as util from "./util.js";

const EDIT_BUTTON_SELECTOR = '[role="edit-window-button"]';
const DELETE_BUTTON_SELECTOR = '[role="delete-window-button"]';

const $addDialog = $('#add-dialog');
const $editDialog = $('#edit-dialog');
const $deleteDialog = $('#delete-dialog');
const $addConfirmButton = $('#confirm-add');
const $editConfirmButton = $('#confirm-edit');
const $deleteConfirmButton = $('#confirm-delete');
const $addWindowNameInput = $('#add-window-name');
const $editWindowNameInput = $('#edit-window-name');

const $addErrorMsg = $('#add-error-msg');
const $editErrorMsg = $('#edit-error-msg');
const $deleteErrorMsg = $('#delete-error-msg');

let windowId;

function setWindowID(target, selector) {
    const $button = $(target).closest(selector)[0];
    const id = $button.id;
    windowId = id.substr(id.lastIndexOf('-') + 1);
}

function onEditClick(event) {
    setWindowID(event.target, EDIT_BUTTON_SELECTOR);
    $editWindowNameInput.val($('#row-' + windowId).text());
    $editDialog.modal('show');
    $editWindowNameInput.trigger('focus');
}

function onDeleteClick(event) {
    setWindowID(event.target, DELETE_BUTTON_SELECTOR);
    $deleteDialog.modal('show');
}

function onEditConfirm() {
    if ($editConfirmButton.hasClass("disabled")) return;
    if (!$editWindowNameInput[0].checkValidity()) {
        $editErrorMsg.text('Validation error');
        util.show($editErrorMsg);
        return;
    }

    util.disable($editConfirmButton);
    util.hide($editErrorMsg);
    util.postJSON(
        '/post/window-edit',
        {id: parseInt(windowId), name: $editWindowNameInput.val()},
        () => {
            util.enable($editConfirmButton);
            $editDialog.modal('hide');
            $('#row-' + windowId).text($editWindowNameInput.val());
        },
        (error) => {
            $editErrorMsg.text(`Error ${error.status}: ${error.statusText}`);
            util.show($editErrorMsg);
            util.enable($editConfirmButton);
        }
    );
}

function onDeleteConfirm() {
    if ($deleteConfirmButton.hasClass("disabled")) return;

    util.disable($deleteConfirmButton);
    util.hide($deleteErrorMsg);

    util.postJSON(
        '/post/window-delete',
        {id: parseInt(windowId)},
        () => {
            util.enable($deleteConfirmButton);
            $deleteDialog.modal('hide');
            $('#row-' + windowId).parent().remove();
        },
        (error) => {
            $deleteErrorMsg.text(`Error ${error.status}: ${error.statusText}`);
            util.show($deleteErrorMsg);
            util.enable($deleteConfirmButton);
        }
    );
}

function onAddClick() {
    $addDialog.modal('show');
    $addWindowNameInput.trigger('focus');
}

function onAddConfirm() {
    if ($addConfirmButton.hasClass("disabled")) return;

    if (!$addWindowNameInput[0].checkValidity()) {
        $addErrorMsg.text('Validation error');
        util.show($addErrorMsg);
        return;
    }

    util.disable($addConfirmButton);
    util.hide($addErrorMsg);

    util.postJSON(
        '/post/window-add',
        {name: $addWindowNameInput.val()},
        () => {
            util.enable($addConfirmButton);
            $addDialog.modal('hide');
            location.reload();
        },
        (error) => {
            $addErrorMsg.text(`Error ${error.status}: ${error.statusText}`);
            util.show($addErrorMsg);
            util.enable($addConfirmButton);
        }
    );
}

function initAll() {
    $("#add-window").on('click', onAddClick);

    const $t = $('[role="windows-table"]');
    $t.on('click', EDIT_BUTTON_SELECTOR, onEditClick);
    $t.on('click', DELETE_BUTTON_SELECTOR, onDeleteClick);

    $addConfirmButton.on('click', onAddConfirm);
    $editConfirmButton.on('click', onEditConfirm);
    $deleteConfirmButton.on('click', onDeleteConfirm);

    $addDialog.on('hidden.bs.modal', () => {
        util.hide($addErrorMsg);
        util.enable($addConfirmButton);
    });

    $editDialog.on('hidden.bs.modal', () => {
        util.hide($editErrorMsg);
        util.enable($editConfirmButton);
    });

    $deleteDialog.on('hidden.bs.modal', () => {
        util.hide($deleteErrorMsg);
        util.enable($deleteConfirmButton);
    });
}

initAll();