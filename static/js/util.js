export function show($target) {
    $target.removeClass("invisible");
}

export function hide($target) {
    $target.addClass("invisible");
}

export function disable($target) {
    $target.addClass("disabled");
}

export function enable($target) {
    $target.removeClass("disabled");
}

export function getID($target) {
    return Number.parseInt($target.id.substr($target.id.lastIndexOf('-') + 1));
}

export function postJSON(path, data, success, error) {
    return $.ajax(path, {
        type: 'post',
        data: JSON.stringify(data),
        dataType: 'json',
        contentType: 'application/json',
        success: success,
        error: error
    });
}