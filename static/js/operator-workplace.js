import * as util from './util.js';

const tryStateChangeEvent = 'try-operator-state-change';
const stateChangeEvent = 'operator-state-change';
const $currentState = $("#current-state-name");
const $nextStatesParent = $("#card-common");
const $nextStates = $("#card-next-states")[0];
const $serviceText = $("#service-name");
const $serviceH = $("#h-service");
const $clientText = $("#client-name");
const $clientH = $("#h-client");

let lastEventDt;
const socket = io('/operators');

function onNextStateClick(event) {
    const state = util.getID($(event.target)[0]);
    const data = {state};
    socket.emit(tryStateChangeEvent, data);
}


function onServerStateChange(data) {
    // console.log('data from socket', data);
    if (lastEventDt > data.dt) {
        return;
    }

    lastEventDt = data.dt;

    if (data.serviceName) {
        $serviceText.text(data.serviceName);
        util.show($serviceH);
    } else {
        util.hide($serviceH);
    }

    if (data.clientName) {
        $clientText.text(data.clientName);
        util.show($clientH);
    } else {
        util.hide($clientH);
    }

    $currentState.text(data.stateName);

    // remove old states
    let $element;
    while (($element = $nextStates.nextSibling)) {
        $element.remove();
    }

    // make new links from event data
    for (const s of data.nextStates) {
        const $a = $(`<a href="#" role="a-next-state" id="state-next-${s.id}">${s.name}</a>`);
        const $p = $(`<p>`);
        $nextStatesParent.append($a);
        $nextStatesParent.append($p);
    }
}


function initAll() {
    socket.on(stateChangeEvent, onServerStateChange);

    $nextStatesParent.on('click', '[role="a-next-state"]', onNextStateClick);
}

initAll();