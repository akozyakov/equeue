function signOutClick() {
    $.post('/post/sign-out')
        .then(() => {
            location.replace('/');
        });
}

function initAll() {
    $('#button-sign-out').click(signOutClick);
}

initAll();