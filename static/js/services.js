import * as util from './util.js';

const EDIT_BUTTON_SELECTOR = '[role="edit-service-button"]';
const DELETE_BUTTON_SELECTOR = '[role="delete-service-button"]';

const $addDialog = $('#add-dialog');
const $editDialog = $('#edit-dialog');
const $deleteDialog = $('#delete-dialog');
const $addConfirmButton = $('#add-save');
const $editConfirmButton = $('#edit-save');
const $deleteConfirmButton = $('#delete-save');
const $addServiceNameInput = $('#add-service-name');
const $editServiceNameInput = $('#edit-service-name');

const $addErrorMsg = $('#add-error-msg');
const $editErrorMsg = $('#edit-error-msg');
const $deleteErrorMsg = $('#delete-error-msg');

let serviceId;

function setServiceID(target, selector) {
    const $button = $(target).closest(selector)[0];
    const id = $button.id;
    serviceId = id.substr(id.lastIndexOf('-') + 1);
}

function onEditClick(event) {
    setServiceID(event.target, EDIT_BUTTON_SELECTOR);
    $editServiceNameInput.val($('#row-' + serviceId).text());
    $editDialog.modal('show');
    $editServiceNameInput.trigger('focus');
}

function onDeleteClick(event) {
    setServiceID(event.target, DELETE_BUTTON_SELECTOR);
    $deleteDialog.modal('show');
}

function onEditConfirm() {
    if ($editConfirmButton.hasClass("disabled")) return;
    if (!$editServiceNameInput[0].checkValidity()) {
        $editErrorMsg.text('Validation error');
        util.show($editErrorMsg);
        return;
    }

    util.disable($editConfirmButton);
    util.hide($editErrorMsg);

    util.postJSON(
        '/post/service-edit',
        {id: Number.parseInt(serviceId), name: $editServiceNameInput.val()},
        () => {
            util.enable($editConfirmButton);
            $editDialog.modal('hide');
            $('#row-' + serviceId).text($editServiceNameInput.val());
        },
        (error) => {
            $editErrorMsg.text(`Error ${error.status}: ${error.statusText}`);
            util.show($editErrorMsg);
            util.enable($editConfirmButton);
        }
    );
}

function onDeleteConfirm() {
    if ($deleteConfirmButton.hasClass("disabled")) return;

    util.disable($deleteConfirmButton);
    util.hide($deleteErrorMsg);

    util.postJSON(
        '/post/service-delete',
        {id: Number.parseInt(serviceId)},
        () => {
            util.enable($deleteConfirmButton);
            $deleteDialog.modal('hide');
            $('#row-' + serviceId).parent().remove();
        },
        (error) => {
            $deleteErrorMsg.text(`Error ${error.status}: ${error.statusText}`);
            util.show($deleteErrorMsg);
            util.enable($deleteConfirmButton);
        }
    );
}

function onAddClick() {
    $addDialog.modal('show');
    $addServiceNameInput.trigger('focus');
}

function onAddConfirm() {
    if ($addConfirmButton.hasClass("disabled")) return;
    if (!$addServiceNameInput[0].checkValidity()) {
        $addErrorMsg.text('Validation error');
        util.show($addErrorMsg);
        return;
    }

    util.disable($addConfirmButton);
    util.hide($addErrorMsg);

    util.postJSON(
        '/post/service-add',
        {name: $addServiceNameInput.val()},
        () => {
            util.enable($addConfirmButton);
            $addDialog.modal('hide');
            location.reload();
        },
        (error) => {
            $addErrorMsg.text(`Error ${error.status}: ${error.statusText}`);
            util.show($addErrorMsg);
            util.enable($addConfirmButton);
        }
    );
}

function initAll() {
    $("#add-service").on('click', onAddClick);

    const $t = $('[role="services-table"]');
    $t.on('click', EDIT_BUTTON_SELECTOR, onEditClick);
    $t.on('click', DELETE_BUTTON_SELECTOR, onDeleteClick);

    $addConfirmButton.on('click', onAddConfirm);
    $editConfirmButton.on('click', onEditConfirm);
    $deleteConfirmButton.on('click', onDeleteConfirm);

    $addDialog.on('hidden.bs.modal', () => {
        util.hide($addErrorMsg);
        util.enable($addConfirmButton);
    });

    $editDialog.on('hidden.bs.modal', () => {
        util.hide($editErrorMsg);
        util.enable($editConfirmButton);
    });

    $deleteDialog.on('hidden.bs.modal', () => {
        util.hide($deleteErrorMsg);
        util.enable($deleteConfirmButton);
    });
}

initAll();