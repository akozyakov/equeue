import * as util from "./util.js";

const $errorMsg = $('#error-msg');
const $clientNameEdit = $('#client-name');

async function registerClick() {
    util.hide($errorMsg);

    if (!$clientNameEdit[0].checkValidity()) {
        $errorMsg.text('Validation error');
        util.show($errorMsg);
        return;
    }

    try {
        const service = $.map($('#services-list input:checked'), util.getID)[0];

        await util.postJSON(
            '/post/client-register',
            {
                name: $clientNameEdit.val(),
                service: Number.parseInt(service),
            }
        );


        $clientNameEdit.val('');
        location.reload();
    } catch (err) {
        if (err.responseJSON && err.responseJSON.message) {
            $errorMsg.text(err.responseJSON.message);
        } else {
            $errorMsg.text(`${err.status} ${err.statusText}`);
        }
        util.show($errorMsg);
    }
}

function initAll() {
    $('#button-register').click(registerClick);
}

initAll();