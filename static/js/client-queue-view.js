const $queueTable = $('#queue-table-body');

function onQueueChange(data) {
    console.log(data);
    $queueTable.empty();
    for (const c of data) {
        const $row = $queueTable.append('<tr>');
        $row.append(`<td>${c.clientName}</td>`);
        $row.append(`<td>${c.windowName}</td>`);
    }
}


function initAll() {
    const socket = io('/clients');
    socket.on('operator-state-change', onQueueChange);
}

initAll();