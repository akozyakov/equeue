import * as util from './util.js';

const EDIT_BUTTON_SELECTOR = '[role="edit-operator-button"]';
const DELETE_BUTTON_SELECTOR = '[role="delete-operator-button"]';
const CHANGE_PASSWORD_BUTTON_SELECTOR = '[role="change-password-button"]';

const $addDialog = $('#add-dialog');
const $editDialog = $('#edit-dialog');
const $deleteDialog = $('#delete-dialog');
const $changePasswordDialog = $('#change-password-dialog');

const $addConfirmButton = $('#add-confirm');
const $editConfirmButton = $('#edit-confirm');
const $deleteConfirmButton = $('#delete-confirm');
const $changePasswordConfirmButton = $('#change-password-confirm');

const $addOperatorNameInput = $('#add-operator-name');
const $editOperatorNameInput = $('#edit-operator-name');
const $oldPasswordInput = $('#old-operator-password');
const $newPasswordInput = $('#new-operator-password');
const $newPasswordCopyInput = $('#new-operator-password-copy');

const $addErrorMsg = $('#add-error-msg');
const $editErrorMsg = $('#edit-error-msg');
const $deleteErrorMsg = $('#delete-error-msg');
const $changePasswordErrorMsg = $('#change-password-error-msg');

let operatorId;

function setOperatorID(target, selector) {
    const $button = $(target).closest(selector)[0];
    const id = $button.id;
    operatorId = id.substr(id.lastIndexOf('-') + 1);
}

function onEditClick(event) {
    setOperatorID(event.target, EDIT_BUTTON_SELECTOR);
    $editOperatorNameInput.val($('#row-' + operatorId).text());
    $editDialog.modal('show');
    $editOperatorNameInput.trigger('focus');
}

function onDeleteClick(event) {
    setOperatorID(event.target, DELETE_BUTTON_SELECTOR);
    $deleteDialog.modal('show');
}

function onEditConfirm() {
    if ($editConfirmButton.hasClass("disabled")) return;
    if (!$editOperatorNameInput[0].checkValidity()) {
        $editErrorMsg.text('Validation error');
        util.show($editErrorMsg);
        return;
    }

    util.disable($editConfirmButton);
    util.hide($editErrorMsg);

    util.postJSON(
        '/post/operator-edit',
        {id: Number.parseInt(operatorId), name: $editOperatorNameInput.val()},
        () => {
            util.enable($editConfirmButton);
            $editDialog.modal('hide');
            $('#row-' + operatorId).text($editOperatorNameInput.val());
        },
        (error) => {
            $editErrorMsg.text(`Error ${error.status}: ${error.statusText}`);
            util.show($editErrorMsg);
            util.enable($editConfirmButton);
        }
    );
}

function onDeleteConfirm() {
    if ($deleteConfirmButton.hasClass("disabled")) return;

    util.disable($deleteConfirmButton);
    util.hide($deleteErrorMsg);

    util.postJSON(
        '/post/operator-delete',
        {id: Number.parseInt(operatorId)},
        () => {
            util.enable($deleteConfirmButton);
            $deleteDialog.modal('hide');
            $('#row-' + operatorId).parent().remove();
        },
        (error) => {
            $deleteErrorMsg.text(`Error ${error.status}: ${error.statusText}`);
            util.show($deleteErrorMsg);
            util.enable($deleteConfirmButton);
        }
    );
}

function onAddClick() {
    $addDialog.modal('show');
    $addOperatorNameInput.trigger('focus');
}

function onAddConfirm() {
    if ($addConfirmButton.hasClass("disabled")) return;
    if (!$addOperatorNameInput[0].checkValidity()) {
        $addErrorMsg.text('Validation error');
        util.show($addErrorMsg);
        return;
    }

    util.disable($addConfirmButton);
    util.hide($addErrorMsg);

    util.postJSON(
        '/post/operator-add',
        {
            name: $addOperatorNameInput.val(),
            password: $('#add-operator-password').val(),
            passwordCopy: $('#add-operator-password-copy').val()
        },
        () => {
            util.enable($addConfirmButton);
            $addDialog.modal('hide');
            location.reload();
        },
        (error) => {
            if (error.responseJSON && error.responseJSON.message) {
                $addErrorMsg.text(error.responseJSON.message);
            } else {
                $addErrorMsg.text(`Error ${error.status}: ${error.statusText}`);
            }
            util.show($addErrorMsg);
            util.enable($addConfirmButton);
        }
    );
}

function onChangePasswordClick(event) {
    setOperatorID(event.target, CHANGE_PASSWORD_BUTTON_SELECTOR);
    $changePasswordDialog.modal('show');
    $oldPasswordInput.trigger('focus');
}

function onChangePasswordConfirm() {
    if ($changePasswordConfirmButton.hasClass("disabled")) return;
    if (
        !$oldPasswordInput[0].checkValidity() ||
        !$newPasswordInput[0].checkValidity() ||
        !$newPasswordCopyInput[0].checkValidity()
    ) {
        $changePasswordErrorMsg.text('Validation error');
        show($changePasswordErrorMsg);
        return;
    }

    util.disable($changePasswordConfirmButton);
    util.hide($changePasswordErrorMsg);

    util.postJSON(
        '/post/operator-change-password',
        {
            id: Number.parseInt(operatorId),
            oldPassword: $oldPasswordInput.val(),
            newPassword: $newPasswordInput.val(),
            newPasswordCopy: $newPasswordCopyInput.val()
        },
        () => {
            util.enable($changePasswordConfirmButton);
            $changePasswordDialog.modal('hide');
        },
        (error) => {
            if (error.responseJSON && error.responseJSON.message) {
                $changePasswordErrorMsg.text(error.responseJSON.message);
            } else {
                $changePasswordErrorMsg.text(`Error ${error.status}: ${error.statusText}`);
            }
            util.show($changePasswordErrorMsg);
            util.enable($changePasswordConfirmButton);
        }
    );
}

function initAll() {
    $("#add-operator").on('click', onAddClick);

    const $t = $('[role="operators-table"]');
    $t.on('click', EDIT_BUTTON_SELECTOR, onEditClick);
    $t.on('click', DELETE_BUTTON_SELECTOR, onDeleteClick);
    $t.on('click', CHANGE_PASSWORD_BUTTON_SELECTOR, onChangePasswordClick);

    $addConfirmButton.on('click', onAddConfirm);
    $editConfirmButton.on('click', onEditConfirm);
    $deleteConfirmButton.on('click', onDeleteConfirm);
    $changePasswordConfirmButton.on('click', onChangePasswordConfirm);

    $addDialog.on('hidden.bs.modal', () => {
        util.hide($addErrorMsg);
        util.enable($addConfirmButton);
    });

    $editDialog.on('hidden.bs.modal', () => {
        util.hide($editErrorMsg);
        util.enable($editConfirmButton);
    });

    $deleteDialog.on('hidden.bs.modal', () => {
        util.hide($deleteErrorMsg);
        util.enable($deleteConfirmButton);
    });

    $changePasswordDialog.on('hidden.bs.modal', () => {
        util.hide($changePasswordErrorMsg);
        util.enable($changePasswordConfirmButton);
    });
}

initAll();