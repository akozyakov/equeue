import * as util from "./util.js";

const $errorMsg = $('#error-msg');

async function authClick() {
    util.hide($errorMsg);
    try {
        await util.postJSON(
            '/post/admin-auth',
            {
                password: $('#operator-password').val(),
            }
        );
        location.assign('/');
    } catch (err) {
        if (err.responseJSON && err.responseJSON.message) {
            $errorMsg.text(err.responseJSON.message);
        } else {
            $errorMsg.text(`${err.status} ${err.statusText}`);
        }
        util.show($errorMsg);
    }
}

function initAll() {
    $('#button-auth').click(authClick);
}

initAll();
