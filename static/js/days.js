function closeDay() {
    $.post('/post/day-close', {}, () => {
        location.reload();
    });
}

function openDay() {
    $.post('/post/day-open', {}, () => {
        location.reload();
    });
}

function initAll() {
    $("#close-day-link").click(closeDay);
    $("#open-day-link").click(openDay);
}

initAll();