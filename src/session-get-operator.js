import {parse} from "cookie";
import {unsign} from "cookie-signature";
import * as app from "./app-express";

export async function getSessionOperator(cookies) {
    cookies = parse(cookies);
    const sessionId = unsign(cookies[app.sessionCookieName].slice(2), app.sessionSecret);
    const sessionRow = await this.repository.sessionQuery(sessionId);
    if (!sessionRow.rowCount) {
        throw new Error('session not found');
    }
    return sessionRow.rows[0].sess.auth.operatorId;
}
