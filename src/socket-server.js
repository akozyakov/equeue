import * as io from 'socket.io';

import {
    errorEvent,
    allowedStateChanges,
    operatorStateChangeEvent,
    tryOperatorStateChangeEvent,
} from "./model/queue";
import {stateNames, states} from "./model/operator";
import {clientsQueueData} from "./model/client-queue-data";

const operatorStateChangeSocketEvent = 'operator-state-change';

export class SocketServer {
    constructor(httpServer, cache, getSessionOperator) {
        this.cache = cache;
        this.bus = cache.bus;
        this.repository = cache.repository;
        this.getSessionOperator = getSessionOperator;

        this.server = io.listen(httpServer);
        this.operatorsNamespace = this.server.of('/operators');
        this.clientsNamespace = this.server.of('/clients');
        this.operatorsNamespace.on('connection', this.onOperatorConnect.bind(this));
        this.bus.on(operatorStateChangeEvent, this.onQueueStateChange.bind(this));
        this.bus.on(errorEvent, this.onError.bind(this));
    }


    onError(err) {
        this.error = err;
    }


    clearError() {
        delete this.error;
    }


    getOperatorRoom(operator) {
        return 'room-operator-' + operator;
    }


    async onOperatorConnect(socket) {
        socket.operatorId = await this.getSessionOperator(socket.handshake.headers.cookie);
        await socket.join(this.getOperatorRoom(socket.operatorId));

        socket.on('try-operator-state-change', this.onTryClientStateChange(socket).bind((this)));

        socket.on('disconnect', () => delete socket.operatorId);
    }


    // from http client to queue
    // todo validate data?
    onTryClientStateChange(socket) {
        return (data) => {
            const queueData = {...data};
            queueData.dt = new Date();
            queueData.eventId = Symbol();
            queueData.operator = socket.operatorId;
            this.clearError();
            this.bus.emit(tryOperatorStateChangeEvent, queueData);
            if (this.error) {
                console.log('error', this.error);
            }
        };
    }


    sendOperatorsData(event) {
        const operatorData = {...event};
        operatorData.stateName = stateNames[operatorData.state];

        const service = this.cache.services[event.service];
        if (service) {
            operatorData.serviceName = service.name;
        }

        const client = this.cache.clients[event.client];
        if (client) {
            operatorData.clientName = client.name;
        }

        operatorData.nextStates = allowedStateChanges
            .filter(s => s[0] === event.state && s[2])
            .map(s => ({id: s[1], name: stateNames[s[1]]}));

        delete operatorData.operator;

        const room = this.getOperatorRoom(event.operator);
        this.operatorsNamespace.to(room).emit(operatorStateChangeSocketEvent, operatorData);
    }


    sendClientsData(event) {
        if (event.state === states.STATE_WORKING) {
            return;
        }

        this.clientsNamespace.emit(operatorStateChangeSocketEvent, clientsQueueData(10));
    }


    // from queue to http clients
    onQueueStateChange(data) {
        this.sendOperatorsData(data);
        this.sendClientsData(data);
    }
}