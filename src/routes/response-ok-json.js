export const responseJsonOk = fn => (req, res) => {
    res.sendJsonOk = () => res.status(200).send({message: 'ok'});
    return fn(req, res);
};