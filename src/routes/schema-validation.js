import path from "path";
import * as fs from "fs";
import {Validator} from "jsonschema";
import {UserError} from "../model/error";

const validator = new Validator();

export const schemaValidator = async (fn, dir, file) => {
    let schema;
    file = path.join(dir, path.parse(file).name + '-schema.json');
    try {
        schema = await fs.promises.readFile(file, "utf8");
        schema = JSON.parse(schema);
        validator.addSchema(schema);
    } catch (e) {
        //console.log('error reading validation schema', e);
    }

    return async (req, res) => {
        if (schema) {
            res.type('json');
            const result = validator.validate(req.body, schema);
            if (result.errors.length) {
                console.error('validation error', file, result);
                throw new UserError('Invalid input data', 500);
            }
        }

        return await fn(req, res);
    };
};