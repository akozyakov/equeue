import {cache} from "../cache-init";
import {UserError} from "../model/error";
import {States as states} from "../model/day";

export function authLayoutInfo(session) {
    const a = session.auth || {};
    return {
        name: a.name,
        authAdmin: a.adminLogged,
        authOperator: a.operatorId && a.operatorId !== 0,
        signedOff: !a.adminLogged && !a.operatorId,
    };
}

export function operatorId(session) {
    return session.auth.operatorId;
}

export function signedIn(session) {
    return session.auth && session.auth.name !== '' && session.auth.name !== undefined;
}

export function adminAuth(session) {
    const a = session.auth || {};
    a.name = 'admin';
    a.adminLogged = true;
    delete a.operatorId;
    session.auth = a;
}

export function operatorAuth(session, operatorId, operatorName) {
    const a = session.auth || {};
    a.name = operatorName;
    a.operatorId = operatorId;
    a.adminLogged = false;
    session.auth = a;
}

export function signOut(session) {
    const a = session.auth || {};
    delete a.name;
    delete a.operatorId;
    a.adminLogged = false;
    session.auth = a;
}

export function adminCheck(req) {
    if (!req.session.auth.adminLogged) {
        throw new UserError('Admin access only');
    }
}

export function operatorCheck(req) {
    if (!req.session.auth.operatorId) {
        throw new UserError('Operator access only');
    }
}

export function adminCheckWrapper(fn) {
    return (req, res, next) => {
        adminCheck(req, res);
        return fn(req, res, next);
    }
}

export function operatorCheckWrapper(fn) {
    return (req, res, next) => {
        if (
            this.operatorId(req.session) &&
            (!cache.day.id || cache.day.state === states.DAY_CLOSED)
        ) {
            this.signOut(req.session);
        }
        operatorCheck(req, res);
        return fn(req, res, next);
    }
}

export function signOutCheckWrapper(fn) {
    return (req, res, next) => {
        if (signedIn(req.session)) {
            throw new UserError('Already signed in');
        }

        return fn(req, res, next);
    }
}