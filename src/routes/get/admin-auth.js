import {cache} from '../../cache-init';
import {States} from "../../model/day";
import * as session from "../session-auth";

export const handler = session.signOutCheckWrapper(async (req, res) => {
    const auth = session.authLayoutInfo(req.session);
    const data = {
        auth,
        title: 'Sign in as admin',
        openDay: cache.day.state === States.DAY_OPEN,
        windows: cache.windowsArray(),
        services: cache.servicesArray().filter(s => cache.day.services.includes(s.id))
    };
    return res.render('admin-auth', data);
});