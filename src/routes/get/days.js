import {cache} from '../../cache-init';
import * as session from '../session-auth';

export const handler = session.adminCheckWrapper(async function (req, res) {
    const days = await cache.repository.daysGetLast();
    const openDay = await cache.repository.dayOpen();

    let today = new Date();
    today.setHours(0, 0, 0, 0);
    const auth = session.authLayoutInfo(req.session);
    return res.render('days', {
        auth,
        title: 'Edit days',
        days: days.rows,
        isOpen: openDay.rowCount !== 0,
        isCurrent: openDay.rowCount !== 0 && openDay.rows[0].day.getTime() === today.getTime(),
        openDay: openDay.rows[0]
    });
});