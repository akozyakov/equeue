import {cache} from '../../cache-init';
import * as session from '../session-auth';

export const handler = session.adminCheckWrapper(function (req, res) {
    const operators = cache.operatorsArray();
    const auth = session.authLayoutInfo(req.session);
    res.render('operators', {
        auth, operators, title: 'Edit operators',
    });
});