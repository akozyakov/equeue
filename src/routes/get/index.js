import * as session from '../session-auth';

export const handler = function (req, res, next) {
    const auth = session.authLayoutInfo(req.session);
    res.render('index', {auth, title: 'EQueue',});
};