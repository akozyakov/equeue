import * as session from '../session-auth';
import {clientsQueueData} from "../../model/client-queue-data";

export const handler = session.adminCheckWrapper(async function (req, res) {
    return res.render('client-queue-view', {clientsData: clientsQueueData(10)});
});