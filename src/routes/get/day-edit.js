import {cache} from '../../cache-init';
import {States} from "../../model/day";
import * as session from '../session-auth';
import {UserError} from "../../model/error";

export const handler = session.adminCheckWrapper(async (req, res) => {
    await cache.initDay();

    const id = Number.parseInt(req.query.id);
    const day = cache.day;
    if (day.id !== id) {
        throw new UserError('Day not current');
    }
    if (day.state !== States.DAY_OPEN) {
        throw new UserError('Day not opened');
    }

    const services = Object.values(cache.services).map(s => {
        return {
            id: s.id,
            name: s.name,
            in_day: cache.day.services.includes(s.id) ? 1 : 0
        }
    }).sort(cache.nameSort);
    const auth = session.authLayoutInfo(req.session);
    return res.render('day-edit', {
        auth, day, services, title: 'Edit current day',
    });
});