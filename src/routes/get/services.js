import {cache} from '../../cache-init';
import * as session from '../session-auth';

export const handler = session.adminCheckWrapper(function (req, res) {
    const services = cache.servicesArray();
    const auth = session.authLayoutInfo(req.session);
    res.render('services', {
        auth, services, title: 'Edit services',
    });
});