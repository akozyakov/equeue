import {cache} from '../../cache-init';
import * as session from '../session-auth';

export const handler = session.adminCheckWrapper(async function (req, res) {
    const auth = session.authLayoutInfo(req.session);
    const services = cache.day.services.map(s => {
        return {
            id: cache.services[s].id,
            name: cache.services[s].name
        }
    });
    return res.render('client-registration', {services, auth});
});