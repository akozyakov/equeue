import {cache} from '../../cache-init';
import * as session from '../session-auth';

export const handler = session.adminCheckWrapper(function (req, res) {
    const auth = session.authLayoutInfo(req.session);
    const windows = cache.windowsArray();
    res.render('windows', {
        auth, windows, title: 'Edit service windows',
    });
});