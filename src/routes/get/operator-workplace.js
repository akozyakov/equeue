import * as session from "../session-auth";
import {cache} from "../../cache-init";
import {stateNames} from "../../model/operator";
import {allowedStateChanges} from "../../model/queue";

export const handler = session.operatorCheckWrapper(async function (req, res) {
    const auth = session.authLayoutInfo(req.session);
    const operatorId = session.operatorId(req.session);
    const operator = {...cache.operators[operatorId]};
    const qOperator = cache.queue.operators[operatorId];

    operator.window = cache.windows[qOperator.window].name;

    const qClient = qOperator.client;
    if (qClient) {
        operator.service = cache.services[qClient.service].name;
    }

    if (qOperator.client) {
        const clientRow = await cache.repository.clientQuery(qOperator.client.client);
        operator.client = clientRow.rows[0].name;
    }
    operator.state = stateNames[qOperator.state];

    const nextStates = allowedStateChanges
        .filter(s => s[0] === qOperator.state && s[2])
        .map(s => ({id: s[1], name: stateNames[s[1]]}));

    res.render('operator-workplace', {
        auth,
        operator,
        nextStates,
        title: 'Operator workplace',
    });
});