import {commands} from "../../cache-init";
import {ServiceEditCommand} from "../../commands/service-edit";
import * as session from '../session-auth';

export const handler = session.adminCheckWrapper(async function (req, res) {
    const {id, name} = req.body;

    await commands.execute(ServiceEditCommand, id, name);
    res.sendJsonOk();
});