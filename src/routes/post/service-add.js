import {commands} from "../../cache-init";
import {ServiceAddCommand} from "../../commands/service-add";
import * as session from '../session-auth';

export const handler = session.adminCheckWrapper(async function (req, res) {
    const {name} = req.body;

    const id = await commands.execute(ServiceAddCommand, name);
    res.send({id});
});