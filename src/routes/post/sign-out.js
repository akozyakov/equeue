import * as session from '../session-auth';
import {commands} from "../../cache-init";
import {UserError} from "../../model/error";
import {OperatorSignOutCommand} from "../../commands/operator-sign-out";

export const handler = async function (req, res) {
    if (!session.signedIn(req.session)) {
        throw new UserError('Not signed in');
    }

    const operatorId = req.session.auth.operatorId;
    if (operatorId) {
        await commands.execute(OperatorSignOutCommand, operatorId, new Date());
    }

    session.signOut(req.session);
    res.redirect('/');
};