import {commands} from "../../cache-init";
import * as session from "../session-auth";
import {ClientRegisterCommand} from "../../commands/client-register";

export const handler = session.adminCheckWrapper(async function (req, res) {
    const {name, service} = req.body;

    await commands.execute(ClientRegisterCommand, new Date(), name, service);
    res.sendJsonOk();
});