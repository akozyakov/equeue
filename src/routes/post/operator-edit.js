import * as session from '../session-auth';
import {commands} from "../../cache-init";
import {OperatorEditCommand} from "../../commands/operator-edit";

export const handler = session.adminCheckWrapper(async function (req, res) {
    const {id, name} = req.body;

    await commands.execute(OperatorEditCommand, id, name);
    res.sendJsonOk();
});