import * as session from '../session-auth';
import {commands} from "../../cache-init";
import {WindowEditCommand} from "../../commands/window-edit";

export const handler = session.adminCheckWrapper(async function (req, res) {
    const {id, name} = req.body;

    await commands.execute(WindowEditCommand, id, name);
    res.sendJsonOk();
});