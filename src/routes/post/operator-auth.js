import * as session from '../session-auth';
import {cache, commands} from "../../cache-init";
import {OperatorAuthCommand} from "../../commands/operator-auth";
import {UserError} from "../../model/error";

export const handler = session.signOutCheckWrapper(async (req, res) => {
    const {name, password, window} = req.body;
    const services = req.body["services[]"];
    if (!services) {
        throw new UserError('Select at least one service');
    }

    const operators = cache.operatorsArray().filter(o => o.name === name);
    if (!operators) {
        throw new UserError('Operator not found');
    }

    const operator = operators[0];
    await commands.execute(
        OperatorAuthCommand,
        operator.id,
        password,
        window,
        services.map(s => Number.parseInt(s))
    );
    session.operatorAuth(req.session, operator.id, operator.name);
    res.send('Ok');
});