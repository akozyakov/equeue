import {commands} from "../../cache-init";
import * as session from '../session-auth';
import {ServiceDeleteCommand} from "../../commands/service-delete";

export const handler = session.adminCheckWrapper(async function (req, res) {
    const {id} = req.body;
    await commands.execute(ServiceDeleteCommand, id);
    res.sendJsonOk();
});