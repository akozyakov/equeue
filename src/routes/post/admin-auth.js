import * as session from '../session-auth';
import {UserError} from "../../model/error";

export const handler = session.signOutCheckWrapper((req, res) => {
    const {password} = req.body;
    if (password !== 'admin') {
        throw new UserError('Invalid password');
    }

    session.adminAuth(req.session);
    res.sendJsonOk();
});