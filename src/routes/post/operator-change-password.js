import {cache, commands} from "../../cache-init";
import {Operator} from "../../model/operator";
import {OperatorPasswordChangeCommand} from "../../commands/operator-password-change";
import * as session from '../session-auth';
import {UserError} from "../../model/error";

export const handler = session.adminCheckWrapper(async function (req, res) {
    const {id, oldPassword, newPassword, newPasswordCopy} = req.body;
    if (newPassword !== newPasswordCopy) {
        throw new UserError('New password and copy differs');
    }

    const operator = cache.operators[id];
    const oldPasswordHash = Operator.getPasswordHash(oldPassword, operator.salt);
    const newSalt = Operator.generateSalt();
    const newPasswordHash = Operator.getPasswordHash(newPassword, newSalt);

    await commands.execute(OperatorPasswordChangeCommand, id, oldPasswordHash, newPasswordHash, newSalt);
    res.sendJsonOk();
});