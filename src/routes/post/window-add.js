import {commands} from "../../cache-init";
import {WindowAddCommand} from "../../commands/window-add";
import * as session from '../session-auth';

export const handler = session.adminCheckWrapper(async function (req, res) {
    const {name} = req.body;

    const id = await commands.execute(WindowAddCommand, name);
    res.status(200).json({id});
});