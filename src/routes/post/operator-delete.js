import {commands} from "../../cache-init";
import * as session from '../session-auth';
import {OperatorDeleteCommand} from "../../commands/operator-delete";

export const handler = session.adminCheckWrapper(async function (req, res) {
    const {id} = req.body;
    await commands.execute(OperatorDeleteCommand, id);
    res.sendJsonOk();
});