import {commands} from "../../cache-init";
import * as session from '../session-auth';
import {UserError} from "../../model/error";
import {Operator} from "../../model/operator";
import {OperatorAddCommand} from "../../commands/operator-add";

export const handler = session.adminCheckWrapper(async function (req, res) {
    const {name, password, passwordCopy} = req.body;
    if (password !== passwordCopy) {
        throw new UserError('Password and copy differs');
    }

    const salt = Operator.generateSalt();
    const pwdHash = Operator.getPasswordHash(password, salt);
    const id = await commands.execute(OperatorAddCommand, name, pwdHash, salt);
    res.status(200).json({id});
});