import {commands} from "../../cache-init";
import {DayOpenCommand} from "../../commands/day-open";
import * as session from '../session-auth';

export const handler = session.adminCheckWrapper(async function (req, res) {
    await commands.execute(DayOpenCommand);
    res.sendJsonOk();
});