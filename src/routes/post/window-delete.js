import {commands} from "../../cache-init";
import {WindowDeleteCommand} from "../../commands/window-delete";
import * as session from '../session-auth';

export const handler = session.adminCheckWrapper(async function (req, res) {
    const {id} = req.body;
    await commands.execute(WindowDeleteCommand, id);
    res.sendJsonOk();
});