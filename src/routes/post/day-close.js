import {commands} from "../../cache-init";
import {DayCloseCommand} from "../../commands/day-close";
import * as session from '../session-auth';

export const handler = session.adminCheckWrapper(async function (req, res) {
    await commands.execute(DayCloseCommand);
    res.sendJsonOk();
});