import {commands} from "../../cache-init";
import * as session from '../session-auth';
import {DayEditCommand} from "../../commands/day-edit";

export const handler = session.adminCheckWrapper(async function (req, res) {
    const services = Array.from(req.body["services"]);
    await commands.execute(DayEditCommand, services);
    res.sendJsonOk();
});