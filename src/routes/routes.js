import * as fs from "fs";
import path from "path";
import express from 'express';
import {schemaValidator} from "./schema-validation";
import {responseJsonOk} from "./response-ok-json";

const allRouter = express.Router();

const userErrorWrapper = fn => async (req, res) => {
    try {
        return await fn(req, res);
    } catch (err) {
        // if (err instanceof UserError) {
        console.error('Unhandled exception', err);
        res.status(err.status || 500).send({message: err.message});
    }
};


async function initFromFiles(router, filesPath, url = '', handlerWrappers) {
    const method = filesPath.split('/').pop();
    const dir = method + '/';
    url = url || '/' + dir;

    const files = await fs.promises.readdir(filesPath);
    for (const file of files) {
        if (!file.endsWith('.js')) continue;

        let fileName = path.parse(file).name;
        let handler = require('./' + dir + fileName).handler;
        for (const wrapper of handlerWrappers) {
            handler = await wrapper(handler, filesPath, file);
        }

        let name = fileName;
        if (name === 'index' && method === "get") {
            name = "";
        }

        router[method](url + name, handler);
    }
}

initFromFiles(allRouter, './src/routes/post', '', [responseJsonOk, schemaValidator, userErrorWrapper]);
initFromFiles(allRouter, './src/routes/get', '/', [userErrorWrapper]);

module.exports = allRouter;