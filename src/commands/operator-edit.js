import {UserError} from "../model/error";

export class OperatorEditCommand {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }

    async execute(cache) {
        const s = cache.factory.createOperator(this.id, this.name);
        const data = await s.save();
        if (!data.rowCount) {
            throw new UserError(`cannot edit operator ${this.id}, no records found`);
        }
    }
}