export class ClientAssignedCommand {
    constructor(id, dt, operator, window) {
        Object.assign(this, {id, dt, operator, window});
    }

    execute(cache) {
        return cache.repository.clientQueueAssigned(this.id, this.dt, this.operator, this.window);
    }

}