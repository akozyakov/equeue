import {UserError} from "../model/error";

export class OperatorDeleteCommand {
    constructor(id) {
        this.id = id;
    }

    async execute(cache) {
        const data = await cache.repository.operatorDelete(this.id);
        if (!data.rowCount) {
            throw new UserError('operator not deleted, no record in DB');
        }
    }
}