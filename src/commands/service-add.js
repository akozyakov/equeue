export class ServiceAddCommand {
    constructor(name) {
        this.name = name;
    }

    async execute(cache) {
        const data = await cache.repository.serviceAdd(this.name);
        const id = data.rows[0].id;
        this.id = id;
        return id;
    }
}