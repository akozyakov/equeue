export class OperatorAddCommand {
    constructor(name, password, salt) {
        this.name = name;
        this.password = password;
        this.salt = salt;
    }

    execute(cache) {
        return cache.repository.operatorAdd(this.name, this.password, this.salt)
            .then((data) => {
                const id = data.rows[0].id;
                this.id = id;
                return id;
            });
    }
}