export class Commands {
    constructor(cache, bus) {
        this.bus = bus;
        this.cache = cache;
    }


    async executeCommand(command) {
        if (command.execute) {
            return command.execute(this.cache, this.bus);
        }

        if (command.executeInTransaction) {
            const transaction = await this.cache.repository.db.connect();
            try {
                await transaction.query("begin");
                const result = await command.executeInTransaction(transaction, this.cache, this.bus);
                await transaction.query("commit");
                return result;
            } catch (err) {
                await transaction.query("rollback");
                throw err;
            } finally {
                transaction.release();
            }
        }

        throw new Error(`no execute or executeInTransaction methods on command ${command.name}`);
    }


    async execute(CommandName, ...params) {
        const command = new CommandName(...params);
        const result = await this.executeCommand(command);
        this.bus.emit('command', command);
        return result;
    }
}
