import {UserError} from "../model/error";

export class ServiceDeleteCommand {
    constructor(id) {
        this.id = id;
    }

    async execute(cache) {
        const data = await cache.repository.serviceDelete(this.id);
        if (!data.rowCount) {
            throw new UserError('service not deleted, no record in DB');
        }
    }
}