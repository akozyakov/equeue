import {DayOpenCommand} from "./day-open";

it("Repository.Init", async () => {
    const c = new DayOpenCommand();
    const modelMock = {
        repository: {
            dayOpen: () => {
                return {rowCount: 0};
            },
        },
        factory: {
            createDay: (dt) => {
                return dt;
            }
        }
    };
    const p = await c.execute(modelMock);

    expect(cache.day).toBe(new Date());

});