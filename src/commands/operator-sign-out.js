import {operatorSignOutEvent} from "../model/queue";

export class OperatorSignOutCommand {
    constructor(operatorId, endDt) {
        Object.assign(this, {operatorId, endDt});
    }

    executeInTransaction(transaction, cache, bus) {
        bus.emit(operatorSignOutEvent, {operator: this.operatorId});
        // cacheEvents.throwLastError();

        const today = new Date();
        today.setHours(0, 0, 0, 0);
        const p1 = cache.repository.operatorStateCloseCurrent(transaction, this.operatorId, this.endDt);
        const p2 = cache.repository.operatorAuthCloseCurrent(transaction, this.operatorId, this.endDt);
        const p3 = cache.repository.operatorQueueCloseCurrent(transaction, this.operatorId, this.endDt, today);
        return Promise.all([p1, p2, p3]);
    }
}