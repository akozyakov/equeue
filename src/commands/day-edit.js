import {States} from "../model/day";
import {UserError} from "../model/error";

export class DayEditCommand {
    constructor(services) {
        this.services = services;
    }

    async execute(cache) {
        if (cache.day.state === States.DAY_CLOSED) {
            throw new UserError('current day is closed');
        }

        if (!cache.today(cache.day.day)) {
            throw new UserError('no current day');
        }

        for (const s of this.services) {
            if (!cache.services[s]) {
                throw new UserError(`wrong service id: ${s}`);
            }
        }

        await cache.repository.dayServicesDelete(cache.day.id);
        return cache.repository.dayServicesAdd(cache.day.id, this.services);
    }
}