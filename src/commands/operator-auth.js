import {Operator} from "../model/operator";
import {operatorAuthEvent} from "../model/queue";
import {cacheEvents} from "../cache-init";
import {UserError} from "../model/error";

export class OperatorAuthCommand {
    constructor(id, password, window, services) {
        this.id = id;
        this.password = password;
        this.window = window;
        this.services = services;
    }

    async executeInTransaction(transaction, cache, bus) {
        const operator = cache.operators[this.id];
        if (Operator.getPasswordHash(this.password, operator.salt) !== operator.password) {
            throw new UserError('Invalid password');
        }

        this.operator = this.id;
        bus.emit(operatorAuthEvent, this);
        cacheEvents.throwLastError();

        const dt = new Date();
        await cache.repository.operatorStateCloseCurrent(transaction, this.id, dt);
        await cache.repository.operatorAuthCloseCurrent(transaction, this.id, dt);

        const data = await cache.repository.operatorAuthAdd(transaction, this.id, dt, this.window);
        const authId = data.rows[0].id;

        await cache.repository.operatorAuthServicesAdd(transaction, authId, this.services);
    }
}