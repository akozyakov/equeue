import {UserError} from "../model/error";

export class WindowDeleteCommand {
    constructor(id) {
        this.id = id;
    }

    async execute(cache) {
        const data = await cache.repository.windowDelete(this.id);
        if (!data.rowCount) {
            throw new UserError('window not deleted, no record in DB');
        }
    }
}