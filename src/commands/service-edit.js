import {UserError} from "../model/error";

export class ServiceEditCommand {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }

    async execute(cache) {
        const service = cache.factory.createService(this.id, this.name);
        const data = await service.save();
        if (!data.rowCount) {
            throw new UserError(`cannot edit service ${this.id}, no records found`);
        }
    }
}