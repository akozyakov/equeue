import {UserError} from "../model/error";

export class DayCloseCommand {
    constructor() {
    }

    async executeInTransaction(transaction, cache) {
        const openDay = await cache.repository.dayOpen();
        if (!openDay.rowCount)
            throw new UserError('Open day not found');

        const dayId = openDay.rows[0].id;
        this.dayId = dayId;

        await cache.repository.dayCloseWithQueue(transaction, dayId);
        await cache.repository.dayCloseWithOperator(transaction, dayId);
        await cache.repository.dayCloseWithOperatorAuth(transaction, openDay.rows[0].day);
        await cache.repository.dayClose(transaction, dayId);
    }
}