import {UserError} from "../model/error";

export class OperatorPasswordChangeCommand {
    constructor(id, oldPassword, newPassword, newSalt) {
        this.id = id;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.newSalt = newSalt;
    }

    async execute(cache) {
        const operator = cache.operators[this.id];
        if (operator.password !== this.oldPassword) {
            throw new UserError('Wrong old password');
        }

        return cache.repository.operatorPasswordSet(this.id, this.newPassword, this.newSalt);
    }
}