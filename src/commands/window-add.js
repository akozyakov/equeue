export class WindowAddCommand {
    constructor(name) {
        this.name = name;
    }

    async execute(cache) {
        const data = await cache.repository.windowAdd(this.name);
        const id = data.rows[0].id;
        this.id = id;
        return id;
    }
}