export class OperatorStateChangeCommand {
    constructor(operator, state, dt) {
        this.dt = dt;
        this.state = state;
        this.operator = operator;
    }

    async executeInTransaction(transaction, cache) {
        await cache.repository.operatorStateClose(transaction, this.operator, this.dt);
        return cache.repository.operatorStateAdd(transaction, this.operator, this.dt, this.state);
    }
}