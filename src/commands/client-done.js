export class ClientDoneCommand {
    constructor(id, dt) {
        Object.assign(this, {id, dt});
    }

    execute(cache) {
        return cache.repository.clientQueueDone(this.id, this.dt);
    }
}