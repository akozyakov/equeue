import {clientRegisteredEvent} from "../model/queue";
import {cacheEvents} from "../cache-init";

export class ClientRegisterCommand {
    constructor(registerTime, name, service) {
        Object.assign(this, {name, service, registerTime});
    }

    async execute(cache, bus) {
        const data = await cache.repository.clientFind(this.name);
        let clientId;
        if (data.rowCount) {
            clientId = data.rows[0].id;
        } else {
            const data = await cache.repository.clientAdd(this.name);
            clientId = data.rows[0].id;
        }

        this.client = clientId;
        bus.emit(clientRegisteredEvent, this);
        cacheEvents.throwLastError();

        const queueRow = await cache.repository.clientQueueAdd(this.registerTime, this.client, this.service);
        this.id = queueRow.rows[0].id;
    }
}