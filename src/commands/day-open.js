import {UserError} from "../model/error";

export class DayOpenCommand {
    constructor(day) {
        if (!day) {
            day = new Date();
            day.setHours(0, 0, 0, 0);
        }
        this.dt = day;
    }

    async execute(cache) {
        const openDay = await cache.repository.dayOpen();
        if (openDay.rowCount) {
            throw new UserError('Open day already exist');
        }

        const day = cache.factory.createDay(this.dt);
        await day.load();
        if (day.id) {
            throw new UserError('Current day already exist');
        }

        const dayData = await cache.repository.dayAdd(this.dt);
        day.id = dayData.rows[0].id;
        this.day = day;
    }
}