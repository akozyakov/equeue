export class ClientLostCommand {
    constructor(id, dt) {
        Object.assign(this, {id, dt});
    }

    execute(cache) {
        return cache.repository.clientQueueLost(this.id, this.dt);
    }
}