import {UserError} from "../model/error";

export class WindowEditCommand {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }

    async execute(cache) {
        const w = cache.factory.createWindow(this.id, this.name);
        const data = await w.save();
        if (!data.rowCount) {
            throw new UserError(`cannot edit window ${this.id}, no records found`);
        }
    }
}