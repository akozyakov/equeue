select
  s.id,
  s.name,
  case
    when ds.service is not null then 1
    else 0
  end in_day
from eq.service s
left join eq.day_service ds on ds.day=$1 and ds.service=s.id
order by s.name