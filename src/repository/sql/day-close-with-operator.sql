update eq.operator_state s
set state=2
from eq.day d
where d.id = $1
  and s.dt between d.day and d.day + 1
  and 1 = 1