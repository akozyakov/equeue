select c.id, c.name
from eq.queue q
join eq.client c on c.id=q.client
where
    q.reg_dt between $1 and $1+interval '1 days' and
    q.cancel_dt is null and
    q.end_dt is null