update eq.queue q
set cancel_dt=d.day+1
from eq.day d
where
    d.id=$1 and
    q.reg_dt between d.day and d.day+1 and
    q.cancel_dt is null and
    q.end_dt is null