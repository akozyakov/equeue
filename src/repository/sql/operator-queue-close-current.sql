update eq.queue q
set end_dt=$2
where
  q.reg_dt between $3 and $3 + interval '1 days' and
  q.operator=$1 and
  q.end_dt is null and
  q.cancel_dt is null