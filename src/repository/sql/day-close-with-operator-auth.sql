update eq.operator_auth a
set end_dt=$1+interval '1 days'
where a.start_dt between $1 and $1+interval '1 days'