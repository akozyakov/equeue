select o.operator, s.service
from eq.operator_state o
join eq.operator_auth a on a.operator=o.operator and a.end_dt is null
join eq.operator_auth_service s on s.operator_auth=a.id
where
  o.dt between $1 and $1+interval '1 days' and
  o.end_dt is null