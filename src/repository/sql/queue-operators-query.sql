select o.operator, o.dt, o.state, q.client, a.window_
from eq.operator_state o
join eq.operator_auth a on a.operator=o.operator and a.end_dt is null
-- sure of only 1 record found here?
left join eq.queue q on
  q.reg_dt between $1 and $1+interval '1 days' and
  q.operator=o.operator and
  q.cancel_dt is null and
  q.end_dt is null
where
  o.dt between $1 and $1+interval '1 days' and
  o.end_dt is null