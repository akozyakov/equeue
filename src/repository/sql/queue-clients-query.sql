select
  q.id,q.client,q.reg_dt,q.service,q.window_,q.operator
from eq.queue q
where
  q.reg_dt between $1 and $1+interval '1 days' and
  q.cancel_dt is null and
  q.end_dt is null