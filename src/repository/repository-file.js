import fs from "fs";
import path from "path";
import util from "util";
import * as pg from "pg";

const readdir = util.promisify(fs.readdir);
const readFile = util.promisify(fs.readFile);

function convertFileToMethodName(fileName) {
    return fileName.split("-")
        .map((w, i) => i === 0 ? w : w.substr(0, 1).toUpperCase() + w.substr(1))
        .join('');
}

export class RepositoryFile {
    constructor(db, path) {
        this.db = db;
        this.path = path;
    }

    async getReaders() {
        let readers = [];
        let filesData = [];
        const files = await readdir(this.path);
        files.filter(f => f.endsWith(".sql")).forEach(file => {
            const reader = readFile(path.join(this.path, file), {encoding: "utf8"});
            reader.then((data) => {
                const fileName = path.parse(file).name;
                filesData.push({fileName, data});
            });
            readers.push(reader);
        });
        return {readers, filesData};
    }

    makeQueries(readers, filesData) {
        return Promise.all(readers).then(() => {
                for (let fileData of filesData) {
                    this[convertFileToMethodName(fileData.fileName)] = async (...args) => {
                        if (args && args[0] instanceof pg.Client) {
                            // run query in transaction
                            return args[0].query(fileData.data, args.slice(1));
                        }
                        return this.db.query(fileData.data, args);
                    }
                }
            }
        );
    }

    async init() {
        const {readers, filesData} = await this.getReaders();
        return this.makeQueries(readers, filesData);
    }
}