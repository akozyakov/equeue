import {RepositoryFile} from "./repository-file"

it("RepositoryFile.Init", async () => {
    const client = {
        release: () => {
        }
    };

    const db = {
        query: (...args) => Promise.resolve(args),
        connect: () => client
    };
    const r = new RepositoryFile(db, "./src/repository/sql");
    await r.init();
    const data = await r.dayQuery("p1");
    expect(data).toStrictEqual(["select d.* from eq.day d where d.day=$1", ["p1"]]);
});