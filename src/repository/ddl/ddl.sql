create schema eq;

drop table if exists eq.day;
create table eq.day
(
    id    serial primary key,
    day   date not null,
    state int  not null check (state in (1, 2)),
    unique (day)
);

drop table if exists eq.service;
create table eq.service
(
    id   serial primary key,
    name text not null
);

drop table if exists eq.operator;
create table eq.operator
(
    id   serial primary key,
    name text not null,
    pwd  text not null,
    salt text not null
);

drop table if exists eq.day_service;
create table eq.day_service
(
    day     int,
    service int,
    primary key (day, service)
);

alter table eq.day_service
    add constraint
        fk_day_service_day foreign key (day) references eq.day (id);

alter table eq.day_service
    add constraint
        fk_day_service_service foreign key (service) references eq.service (id);

drop table if exists eq.window_;
create table eq.window_
(
    id   serial primary key,
    name text not null
);

drop table if exists eq.client;
create table eq.client
(
    id   serial primary key,
    name text not null
);

drop table if exists eq.operator_state;
create table
    eq.operator_state
(
    id       serial primary key,
    operator int       not null references eq.operator (id),
    dt       timestamp not null,
    state    int       not null check (state in (1, 2, 3, 4)),
    end_dt   timestamp,
    check (end_dt >= dt)
);

drop table if exists eq.operator_auth;
create table eq.operator_auth
(
    id       serial primary key,
    operator int       not null references eq.operator (id),
    start_dt timestamp not null,
    window_  int       not null references eq.window_ (id),
    end_dt   timestamp,
    check (end_dt >= start_dt)
);

drop table if exists eq.operator_auth_service;
create table eq.operator_auth_service
(
    id            serial primary key,
    operator_auth int not null references eq.operator_auth (id),
    service       int not null references eq.service (id),
    unique (operator_auth, service)
);

drop table if exists eq.queue;
create table eq.queue
(
    id        serial primary key,
    reg_dt    timestamp not null,
    client    int       not null references eq.client (id),
    service   int       not null references eq.service (id),
    start_dt  timestamp,
    window_   int       references eq.window_ (id),
    operator  int       references eq.operator (id),
    cancel_dt timestamp,
    end_dt    timestamp,
    check (case when cancel_dt is not null then 1 else 0 end + case when end_dt is not null then 1 else 0 end <= 1)
);

alter table eq.queue add constraint  queue_start_dt check(start_dt>=reg_dt);
alter table eq.queue add constraint  queue_cancel_dt check(cancel_dt>=start_dt);
alter table eq.queue add constraint  queue_end_dt check(end_dt>=start_dt);

CREATE TABLE "session"
(
    "sid"    varchar      NOT NULL COLLATE "default",
    "sess"   json         NOT NULL,
    "expire" timestamp(6) NOT NULL
)
    WITH (OIDS= FALSE);

ALTER TABLE "session"
    ADD CONSTRAINT "session_pkey" PRIMARY KEY ("sid") NOT DEFERRABLE INITIALLY IMMEDIATE;

CREATE INDEX "IDX_session_expire" ON "session" ("expire");
