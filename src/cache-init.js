import {Pool} from "pg";
import {EventEmitter} from "events";
import {RepositoryFile} from "./repository/repository-file"
import {ModelFactory} from "./model/factory";
import {Cache} from "./model/cache";
import {Commands} from "./commands/commands";
import {CacheEvents} from "./model/cache-events";

const pgPath = {
    user: "postgres",
    host: "localhost",
    database: "Test1",
    password: "postgres",
    port: 5432
};

export const pgPool = new Pool(pgPath);
export const repository = new RepositoryFile(pgPool, "./src/repository/sql/");
export const eventBus = new EventEmitter();
export let cache;
export let factory;
export let commands;
export let cacheEvents;

export async function cacheInit() {
    await repository.init();

    factory = new ModelFactory(repository);
    cache = new Cache(eventBus, repository, factory, 1000 * 60 * 2 /* 2 minute */);
    commands = new Commands(cache, eventBus);
    cacheEvents = new CacheEvents(cache, eventBus, commands);
    return cache.init()
        .catch((err) => {
            console.log(err);
            process.exit(1);
        });
}
