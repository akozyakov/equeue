export class Service {
    constructor(repository, id, name) {
        this.repository = repository;
        this.id = id;
        this.name = name
    }

    async initById(id) {
        const serviceQuery = this.repository.serviceGet(id);
        return serviceQuery.then(data => {
            if (!data.rowCount) return;
            this.id = id;
            this.name = data.rows[0].name;
        });
    }

    async save() {
        return this.repository.serviceUpdate(this.id, this.name);
    }

    async delete() {
        return this.repository.serviceDelete(this.id);
    }
}