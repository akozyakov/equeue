import {states} from "./operator";

export const errorEvent = Symbol('error');
export const operatorAuthEvent = Symbol('operator auth');
export const operatorSignOutEvent = Symbol('operator sign out');
export const operatorStateChangeEvent = Symbol('operator state change');
export const tryOperatorStateChangeEvent = Symbol('try operator state change');
export const clientRegisteredEvent = Symbol('register client');
export const clientAssignedEvent = Symbol('client assigned');
export const clientLostEvent = Symbol('client lost');
export const clientDoneEvent = Symbol('client done');
export const clientRegisteredIdEvent = Symbol('register client id');

/*
ручные смены: З-Г,Г-З,Ж-Р,Р-З,Р-Г
автоматические: Г-Ж,Ж-З
*/
export const allowedStateChanges = [
    [undefined, states.STATE_BUSY, false], // operator auth
    [states.STATE_BUSY, states.STATE_READY, true], // operator ready
    [states.STATE_READY, states.STATE_BUSY, true], // operator busy
    [states.STATE_WAITING, states.STATE_WORKING, true], // operator starts working
    [states.STATE_WORKING, states.STATE_BUSY, true], // operator finished working and busy
    [states.STATE_WORKING, states.STATE_READY, true], // operator finished working and ready
    [states.STATE_READY, states.STATE_WAITING, false], // client assigned to operator, auto
    [states.STATE_WAITING, states.STATE_BUSY, false], // client lost, waiting timeout, auto
];

// todo check start time for current day - client reg, operator ready
export class Queue {

    constructor(bus, services, lostTimeout) {
        this.bus = bus;
        this.services = services;
        this.lostTimeout = lostTimeout;
        // here is unassigned, waiting and in work clients
        this.clients = {};
        this.operators = {};
        this.bus.on(operatorAuthEvent, this.onOperatorAuth.bind(this));
        this.bus.on(operatorSignOutEvent, this.onOperatorSignOut.bind(this));
        this.bus.on(operatorStateChangeEvent, this.onOperatorStateChange.bind(this));
        this.bus.on(tryOperatorStateChangeEvent, this.onTryOperatorStateChange.bind(this));
        this.bus.on(clientRegisteredEvent, this.onClientRegistered.bind(this));
        this.bus.on(clientRegisteredIdEvent, this.onClientRegisteredId.bind(this));
    }


    generateEventId(description) {
        return Symbol(description);
    }


    error(event, eventId, msg) {
        this.bus.emit(errorEvent, {event, eventId, msg});
    }


    onOperatorAuth({eventId, operator, window, services}) {
        if (!services.length) {
            this.error(operatorAuthEvent, eventId, 'services cannot be empty');
            return;
        }
        for (const service of services) {
            if (!this.services.includes(service)) {
                this.error(operatorAuthEvent, eventId, `service ${service} not in day`);
                return;
            }
        }

        for (const op of Object.values(this.operators)) {
            if (op.window === window) {
                this.error(operatorAuthEvent, eventId, `window ${op.window} already in use`);
                return;
            }
            if (op.operator === operator) {
                this.error(operatorAuthEvent, eventId, `operator ${op.operator} already in auth`);
                return;
            }
        }

        this.operators[operator] = {operator, window, services};
        this.bus.emit(operatorStateChangeEvent,
            {eventId: this.generateEventId(), operator, state: states.STATE_BUSY, dt: new Date()}
        );
    }


    onOperatorSignOut({eventId, operatorId}) {
        const operator = this.operators[operatorId];
        if (!operator) {
            this.error(operatorSignOutEvent, eventId, 'not logged in');
            return;
        }

        if (operator.client) {
            this.error(operatorSignOutEvent, eventId, 'client assigned to operator');
            return;
        }

        delete this.operators[operator];
    }


    checkAllowedStateChange(fromState, toState) {
        for (const c of allowedStateChanges) {
            if (c[0] === fromState && c[1] === toState) {
                return true;
            }
        }

        return false;
    }


    canStateChange(eventData) {
        const {eventId, operator, state} = eventData;
        if (!this.operators[operator]) {
            this.error(operatorStateChangeEvent, eventId, `operator ${operator} not found`);
            return false;
        }

        const currentState = this.operators[operator].state;
        if (!this.checkAllowedStateChange(currentState, state, eventId)) {
            this.error(operatorStateChangeEvent, eventId, 'change not allowed');
            return false;
        }

        return true;
    }


    onTryOperatorStateChange(eventData) {
        if (!this.canStateChange(eventData)) {
            return;
        }

        if (eventData.state === states.STATE_WORKING) {
            const operator = this.operators[eventData.operator];
            eventData.window = operator.window;
            eventData.client = operator.client.client;
            eventData.service = operator.client.service;
        }

        this.bus.emit(operatorStateChangeEvent, eventData);
    }


    onOperatorStateChange(eventData) {
        const {operator, state} = eventData;
        if (!this.canStateChange(eventData)) {
            return;
        }

        const qOperator = this.operators[operator];
        const currentState = qOperator.state;
        qOperator.state = state;
        if (currentState === states.STATE_WORKING) {
            this.bus.emit(clientDoneEvent, {
                eventId: this.generateEventId(),
                id: qOperator.client.id,
                client: qOperator.client.client,
                dt: new Date(),
            });
        }
        if ([states.STATE_BUSY, states.STATE_READY].includes(state) && qOperator.client) {
            delete this.clients[qOperator.client.client];
            delete qOperator.client;
        }
        if (currentState === states.STATE_WAITING && this.operators[operator].timeout) {
            clearTimeout(this.operators[operator].timeout);
            delete this.operators[operator].timeout;
        }
        if (state === states.STATE_READY) {
            this.checkClientAssigned();
        }
    }


    // when client added or operator is ready - can assign the client to him
    checkClientAssigned() {
        // todo lock?
        const clients = Object.values(this.clients)
            .sort((c1, c2) => c1.registerTime.getTime() - c2.registerTime.getTime());

        for (const client of clients) {
            if (client.operator) continue;

            // find ready operator with needed service
            const operators = Object.values(this.operators).filter(
                o => o.services.includes(client.service) && o.state === states.STATE_READY
            );
            if (!operators.length) continue;

            // operator found, set client to operator
            const operator = operators[0];
            client.window = operator.window;
            client.operator = operator;
            operator.client = client;
            this.bus.emit(operatorStateChangeEvent,
                {
                    eventId: this.generateEventId(),
                    operator: operator.operator,
                    state: states.STATE_WAITING,
                    window: operator.window,
                    client: client.client,
                    service: client.service,
                    dt: new Date()
                }
            );
            this.bus.emit(clientAssignedEvent, {
                eventId: this.generateEventId(),
                id: client.id,
                client: client.client,
                operator: operator.operator,
                window: operator.window,
                dt: new Date()
            });

            // prepare for client lost
            operator.timeout = setTimeout(() => {
                    delete operator.timeout;
                    this.bus.emit(operatorStateChangeEvent, {
                        eventId: this.generateEventId(),
                        operator: operator.operator,
                        state: states.STATE_BUSY,
                        dt: new Date(),
                    });
                    this.bus.emit(clientLostEvent, {
                        eventId: this.generateEventId(),
                        id: client.id,
                        client: client.client,
                        dt: new Date(),
                    });
                },
                this.lostTimeout);
        }
    }


    onClientRegistered({eventId, id, client, service, registerTime}) {
        if (!this.services.includes(service)) {
            this.error(clientRegisteredEvent, eventId, `service ${service} not in current day`);
            return;
        }

        if (this.clients[client]) {
            this.error(clientRegisteredEvent, eventId, `client ${client} already in queue`);
            return;
        }

        this.clients[client] = {id, client, service, registerTime};
    }


    onClientRegisteredId({client, id}) {
        this.clients[client].id = id;
        this.checkClientAssigned();
    }


    initFromData(operators, clients) {
        this.operators = operators;
        this.clients = clients;
    }


    /*
    Один метод - получить текущее состояние для окна клиентов
    надо показывать только назначенных и ожидающих
      по времени вниз
    */
    getCurrentClients(limit) {
        // here is unassigned, waiting and in work clients
        return Object.values(this.clients)
            .filter(c => !c.operator || c.operator.state !== states.STATE_WORKING)
            .map(c => ({
                client: c.client,
                window: c.window,
                operator: c.operator && c.operator.operator,
                state: c.operator && c.operator.state,
                registerTime: c.registerTime
            }))
            .sort((c1, c2) => c1.registerTime.getTime() - c2.registerTime.getTime())
            .slice(0, limit - 1);
    }
}
