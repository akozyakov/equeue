import {States} from "./day";
import {
    errorEvent,
    operatorStateChangeEvent,
    clientAssignedEvent,
    clientLostEvent,
    clientDoneEvent,
    clientRegisteredIdEvent,
} from "./queue";
import {OperatorStateChangeCommand} from "../commands/operator-state-change";
import {ClientAssignedCommand} from "../commands/client-assigned";
import {ClientDoneCommand} from "../commands/client-done";
import {ClientLostCommand} from "../commands/client-lost";
import {UserError} from "./error";

export class CacheEvents {
    constructor(cache, bus, commands) {
        this.bus = bus;
        this.cache = cache;
        this.commands = commands;

        this.bus.on('command', (command) => {
            const handler = this['on' + command.constructor.name];
            if (handler) {
                handler.call(this, command);
            }
        });
        this.bus.on(operatorStateChangeEvent, this.onOperatorStateChangeEvent.bind(this));
        this.bus.on(clientAssignedEvent, this.onClientAssignedEvent.bind(this));
        this.bus.on(clientDoneEvent, this.onClientDoneEvent.bind(this));
        this.bus.on(clientLostEvent, this.onClientLostEvent.bind(this));

        this.bus.on(errorEvent, (error) => {
            this.error = error;
        });
    }


    throwLastError() {
        if (this.error) {
            const error = this.error;
            delete this.error;
            throw new UserError(error.msg);
        }
    }


    onOperatorAddCommand(command) {
        this.cache.operators[command.id] =
            this.cache.factory.createOperator(command.id, command.name, command.password, command.salt);
        delete this.cache.operators_;
    }


    onOperatorEditCommand(command) {
        this.cache.operators[command.id].name = command.name;
    }


    onOperatorDeleteCommand(command) {
        delete this.cache.operators[command.id];
        delete this.cache.operators_;
    }


    onOperatorChangePasswordCommand(command) {
        const operator = this.cache.operators[command.id];
        operator.password = command.newPassword;
        operator.salt = command.newSalt;
    }


    onWindowAddCommand(command) {
        this.cache.windows[command.id] =
            this.cache.factory.createWindow(command.id, command.name);
        delete this.cache.windows_;
    }


    onWindowEditCommand(command) {
        this.cache.windows[command.id].name = command.name;
    }


    onWindowDeleteCommand(command) {
        delete this.cache.windows[command.id];
        delete this.cache.windows_;
    }


    onServiceAddCommand(command) {
        this.cache.services[command.id] =
            this.cache.factory.createService(command.id, command.name);
        delete this.cache.services_;
    }


    onServiceEditCommand(command) {
        this.cache.services[command.id].name = command.name;
    }


    onServiceDeleteCommand(command) {
        delete this.cache.services[command.id];
        delete this.cache.services_;
    }


    onCloseDayCommand(command) {
        if (this.cache.day.id === command.dayId) {
            this.cache.day.state = States.DAY_CLOSED;
            this.cache.operatorAuth = {}
        }
    }
    onDayOpenCommand(command) {
        this.cache.day = command.day;
        this.cache.queue.services = command.day.services;
    }


    onDayEditCommand(command) {
        this.cache.day.services = command.services;
        this.cache.queue.services = command.services;
    }


    onClientRegisterCommand(command) {
        const {client, name} = command;
        this.cache.clients[client] = {client, name};
        this.bus.emit(clientRegisteredIdEvent, command);
    }


    onOperatorStateChangeEvent(event) {
        this.commands.execute(OperatorStateChangeCommand, event.operator, event.state, event.dt);
    }


    async onClientAssignedEvent(event) {
        await this.commands.execute(ClientAssignedCommand,
            event.id, event.dt, event.operator, event.window
        );
        delete this.error;
    }


    async onClientDoneEvent(event) {
        await this.commands.execute(ClientDoneCommand, event.id, event.dt);
        delete this.error;
        delete this.cache.clients[event.client];
    }


    async onClientLostEvent(event) {
        await this.commands.execute(ClientLostCommand, event.id, event.dt);
        delete this.error;
        delete this.cache.clients[event.client];
    }
}