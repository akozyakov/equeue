import {queueRead} from "./queue-read";

export class Cache {
    constructor(bus, repository, factory, lostTimeout) {
        Object.assign(this, {bus, repository, factory, lostTimeout});
    }

    async init() {
        await Promise.all([
            this.initOperators(),
            this.initWindows(),
            this.initServices(),
            this.initDay(),
            this.initClients(),
        ]);

        return this.initQueue();
    }

    today() {
        let today = new Date();
        today.setHours(0, 0, 0, 0);
        return today;
    }

    isToday(date) {
        return date.getTime() === this.today().getTime();
    }

    async initDay() {
        if (this.day && this.isToday(this.day.day)) return;

        const day = this.factory.createDay(this.today());
        return day.load()
            .then(() => {
                this.day = day;
            });
    }

    async initOperators() {
        return this.repository.operatorsAll().then(data => {
            const operators = {};
            for (let row of data.rows) {
                operators[row.id] = this.factory.createOperator(row.id, row.name, row.pwd, row.salt);
            }
            this.operators = operators;
        });
    }

    async initWindows() {
        return this.repository.windowsAll().then((data) => {
            const windows = {};
            for (let row of data.rows) {
                windows[row.id] = this.factory.createWindow(row.id, row.name);
            }
            this.windows = windows;
        });
    }

    async initServices() {
        return this.repository.servicesAll()
            .then(data => {
                const services = {};
                for (let row of data.rows) {
                    services[row.id] = this.factory.createService(row.id, row.name);
                }
                this.services = services;
            });
    }


    async initClients() {
        this.clients = {};
        const today = new Date();
        today.setHours(0, 0, 0, 0);
        return this.repository.queueClients(today)
            .then(data => data.rows.forEach(c => this.clients[c.id] = c));
    }


    async initQueue() {
        this.queue = this.factory.createQueue(
            this.bus,
            this.day ? this.day.services : undefined,
            this.lostTimeout
        );
        return queueRead(this.repository)
            .then(({operators, clients}) => this.queue.initFromData(operators, clients));
    }

    nameSort(s1, s2) {
        return s1.name === s2.name ? 0 : (s1.name < s2.name ? -1 : 1);
    }

    operatorsArray() {
        if (!this.operators_) {
            this.operators_ = Object.values(this.operators).sort(this.nameSort);
        }
        return this.operators_;
    }

    windowsArray() {
        if (!this.windows_) {
            this.windows_ = Object.values(this.windows).sort(this.nameSort);
        }
        return this.windows_;
    }

    servicesArray() {
        if (!this.services_) {
            this.services_ = Object.values(this.services).sort(this.nameSort);
        }
        return this.services_;
    }
}
