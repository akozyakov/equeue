import {Day} from "./day";
import {Operator} from "./operator";
import {Window} from "./window";
import {Service} from "./service";
import {Queue} from "./queue";

export class ModelFactory {
    constructor(repository) {
        this.repository = repository;
    }

    createDay(date) {
        return new Day(this.repository, date);
    }

    createOperator(id, name, password, salt) {
        return new Operator(this.repository, id, name, password, salt);
    }

    createWindow(id, name) {
        return new Window(this.repository, id, name);
    }

    createService(id, name) {
        return new Service(this.repository, id, name);
    }

    createQueue(bus, services, timeout) {
        return new Queue(bus, services, timeout);
    }
}
