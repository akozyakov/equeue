import {pbkdf2Sync, randomBytes} from "crypto";

export const states = {
    STATE_BUSY: 1,
    STATE_READY: 2,
    STATE_WAITING: 3,
    STATE_WORKING: 4
};

export const stateNames = [
    '',
    'BUSY - short break',
    'READY - waiting for client assigned',
    'WAITING - client assigned, goes here',
    'WORKING - client here, at service'
];

export class Operator {
    constructor(repository, id, name, password, salt) {
        this.repository = repository;
        this.id = id;
        this.name = name;
        this.password = password;
        this.salt = salt;
    }

    static generateSalt() {
        return randomBytes(16).toString('hex');
    }

    static getPasswordHash(password, salt) {
        return pbkdf2Sync(password, salt, 10000, 64, `sha512`).toString(`hex`);
    }

    async load() {
        const result = this.repository.operatorGet(this.id);
        return result.then(data => {
            const row = data.rows[0];
            this.name = row.name;
            this.password = row.password;
            this.salt = row.salt;
        });
    }

    async save() {
        return this.repository.operatorUpdate(this.id, this.name);
    }

    async delete() {
        return this.repository.operatorDelete(this.id);
    }
}