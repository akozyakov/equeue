export class UserError extends Error {
    constructor(msg, status = 400) {
        super(msg);
        this.status = status;
    }
}