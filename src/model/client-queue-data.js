import {cache} from "../cache-init";

export const clientsQueueData = (limit) => {
    const clientsData = cache.queue.getCurrentClients(limit);
    clientsData.forEach(c => {
        c.clientName = cache.clients[c.client].name;
        const window = cache.windows[c.window];
        c.windowName = window && window.name;
    });
    return clientsData;
};