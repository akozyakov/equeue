export class Window {
    constructor(repository, id, name) {
        this.repository = repository;
        this.id = id;
        this.name = name;
    }

    async load() {
        const windowQuery = this.repository.windowGet(this.id);
        return windowQuery.then(data => {
            if (!data.rowCount) return;
            this['name'] = data.rows[0].name;
        });
    }

    async save() {
        return this.repository.windowUpdate(this.id, this.name);
    }

    async delete() {
        return this.repository.windowDelete(this.id);
    }
}