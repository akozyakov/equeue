import {EventEmitter} from 'events';
import * as queue from './queue';
import {states} from "./operator";

function makeBus() {
    const bus = new EventEmitter();
    const addListener = (event) => {
        bus.on(event, (data) => console.log(event, data));
    };
    addListener(queue.operatorAuthEvent);
    addListener(queue.operatorSignOutEvent);
    addListener(queue.tryOperatorStateChangeEvent);
    addListener(queue.operatorStateChangeEvent);
    addListener(queue.clientRegisteredEvent);
    addListener(queue.clientRegisteredIdEvent);
    addListener(queue.clientAssignedEvent);
    addListener(queue.clientDoneEvent);
    addListener(queue.clientLostEvent);
    addListener(queue.errorEvent);
    return bus;
}

function makeQueue(services, lostTimeout) {
    return new queue.Queue(makeBus(), services, lostTimeout);
}

/*
    + bus to console logger
    one client, two operators
    check window
    check service

    two clients, one operator

    two clients, two operators

    different operators state

    timeout
      client found
      client lost
*/

it("Queue set client id", () => {
    const q = makeQueue([1, 2, 3, 4], 10);
    const b = q.bus;
    const eventId = Symbol();
    let error = {};
    b.on(queue.errorEvent, err => error = err);
    b.emit(queue.clientRegisteredEvent, {client: 1, service: 1, registerTime: new Date()});
    b.emit(queue.operatorAuthEvent, {eventId, operator: 1, window: 1, services: [2, 3]});
    b.emit(queue.clientRegisteredIdEvent, {client: 1, id: 15});

    expect(q.clients[1].id).toBe(15);
});

it("Queue unknown service", () => {
    const q = makeQueue([1, 2, 3, 4], 10);
    const b = q.bus;
    const eventId = Symbol();
    let error = {};
    b.on(queue.errorEvent, err => error = err);
    b.emit(queue.clientRegisteredEvent, {client: 1, service: 1, registerTime: new Date()});
    b.emit(queue.operatorAuthEvent, {eventId, operator: 1, window: 1, services: [2, 3, 5]});

    expect(error).toEqual({eventId, event: queue.operatorAuthEvent, msg: 'service 5 not in day'});
});

it("Queue same window", () => {
    const q = makeQueue([1, 2, 3, 4], 10);
    const b = q.bus;
    const eventId = Symbol();
    let error = {};
    b.on(queue.errorEvent, err => error = err);
    b.emit(queue.clientRegisteredEvent, {client: 1, service: 1, registerTime: new Date()});
    b.emit(queue.operatorAuthEvent, {eventId: 1, operator: 1, window: 1, services: [2, 3]});
    b.emit(queue.operatorAuthEvent, {eventId, operator: 2, window: 1, services: [1, 2]});

    expect(error).toEqual({eventId, event: queue.operatorAuthEvent, msg: 'window 1 already in use'});
});

it("Queue same operator", () => {
    const q = makeQueue([1, 2, 3, 4], 10);
    const b = q.bus;
    const eventId = Symbol();
    let error = {};
    b.on(queue.errorEvent, err => error = err);
    b.emit(queue.clientRegisteredEvent, {client: 1, service: 1, registerTime: new Date()});
    b.emit(queue.operatorAuthEvent, {eventId: 1, operator: 1, window: 1, services: [2, 3]});
    b.emit(queue.operatorAuthEvent, {eventId, operator: 1, window: 2, services: [1, 2]});

    expect(error).toEqual({eventId, event: queue.operatorAuthEvent, msg: 'operator 1 already in auth'});
});

it("Queue no services", () => {
    const q = makeQueue([1, 2, 3, 4], 10);
    const b = q.bus;
    const eventId = Symbol();
    let error = {};
    b.on(queue.errorEvent, err => error = err);
    b.emit(queue.clientRegisteredEvent, {client: 1, service: 1, registerTime: new Date()});
    b.emit(queue.operatorAuthEvent, {eventId, operator: 1, window: 1, services: []});

    expect(error).toEqual({eventId, event: queue.operatorAuthEvent, msg: 'services cannot be empty'});
});

it("Queue wrong state 1", () => {
    const q = makeQueue([1, 2, 3, 4], 10);
    const b = q.bus;
    const eventId = Symbol();
    let error = {};
    b.on(queue.errorEvent, err => error = err);
    b.emit(queue.clientRegisteredEvent, {client: 1, service: 1, registerTime: new Date()});
    b.emit(queue.operatorAuthEvent, {operator: 1, window: 1, services: [2, 3]});
    b.emit(queue.operatorAuthEvent, {operator: 2, window: 2, services: [1, 4]});
    b.emit(queue.tryOperatorStateChangeEvent, {eventId, operator: 1, state: states.STATE_WAITING});

    expect(error).toEqual({eventId, event: queue.operatorStateChangeEvent, msg: 'change not allowed'});
});

it("Queue client assigned", () => {
    const q = makeQueue([1, 2, 3, 4], 0);
    const b = q.bus;
    let eventData;
    b.on(queue.clientAssignedEvent, (data) => {
        if (!eventData)
            eventData = data;
    });
    b.emit(queue.clientRegisteredEvent, {client: 1, service: 1, registerTime: new Date()});
    b.emit(queue.clientRegisteredIdEvent, {client: 1, id: 15});
    b.emit(queue.operatorAuthEvent, {operator: 1, window: 1, services: [2, 3]});
    b.emit(queue.operatorAuthEvent, {operator: 2, window: 2, services: [1, 4]});
    b.emit(queue.tryOperatorStateChangeEvent, {operator: 2, state: states.STATE_READY});

    delete eventData.eventId;
    delete eventData.dt;
    expect(eventData).toEqual({client: 1, operator: 2, window: 2, id: 15});
});

it("Queue client lost", (done) => {
    const q = makeQueue([1, 2, 3, 4], 0);
    const b = q.bus;
    let eventData;
    b.on(queue.clientLostEvent, (data) => {
        if (!eventData)
            eventData = data;
    });
    b.emit(queue.clientRegisteredEvent, {client: 1, service: 1, registerTime: new Date()});
    b.emit(queue.clientRegisteredIdEvent, {client: 1, id: 15});
    b.emit(queue.operatorAuthEvent, {operator: 1, window: 1, services: [2, 3]});
    b.emit(queue.operatorAuthEvent, {operator: 2, window: 2, services: [1, 4]});
    b.emit(queue.tryOperatorStateChangeEvent, {operator: 1, state: states.STATE_READY});
    b.emit(queue.tryOperatorStateChangeEvent, {operator: 2, state: states.STATE_READY});

    setTimeout(() => {
        delete eventData.eventId;
        delete eventData.dt;
        expect(eventData).toEqual({id: 15});
        done();
    }, 20);
});

it("Queue client done", (done) => {
    const q = makeQueue([1, 2, 3, 4], 0);
    const b = q.bus;
    let eventData;
    b.on(queue.clientDoneEvent, (data) => {
        if (!eventData)
            eventData = data;
    });
    b.emit(queue.clientRegisteredEvent, {client: 1, service: 1, registerTime: new Date()});
    b.emit(queue.clientRegisteredIdEvent, {client: 1, id: 15});
    b.emit(queue.operatorAuthEvent, {operator: 1, window: 1, services: [2, 3]});
    b.emit(queue.operatorAuthEvent, {operator: 2, window: 2, services: [1, 4]});
    b.emit(queue.tryOperatorStateChangeEvent, {operator: 2, state: states.STATE_READY});
    b.emit(queue.tryOperatorStateChangeEvent, {operator: 2, state: states.STATE_WORKING});
    b.emit(queue.tryOperatorStateChangeEvent, {operator: 2, state: states.STATE_BUSY});

    setTimeout(() => {
        delete eventData.eventId;
        delete eventData.dt;
        expect(eventData).toEqual({id: 15});
        done();
    }, 20);
});
