export const States = {
    DAY_OPEN: 1,
    DAY_CLOSED: 2
};

export const DAY_CLOSED_ERROR = new Error("cannot modify closed day");

export class Day {
    constructor(repository, day) {
        this.repository = repository;
        this.day = day;
        this.services = [];
    }

    async load(id) {
        let dayRow;
        if (id) {
            dayRow = await this.repository.dayQueryById(id);
        } else {
            dayRow = await this.repository.dayQuery(this.day);
        }

        if (dayRow.rowCount) {
            this.id = dayRow.rows[0].id;
            this.day = dayRow.rows[0].day;
            this.state = dayRow.rows[0].state;

            const servicesRows = await this.repository.dayServicesQuery(this.id);
            this.services = servicesRows.rows.map(r => r.service);
            return this;
        }
        this.state = States.DAY_OPEN;
        return false;
    }

    async addService(service) {
        if ((this.state = States.DAY_CLOSED)) {
            throw DAY_CLOSED_ERROR;
        }

        return this.repository.dayServiceAdd(this.id, service.id).then(() => {
            this.services.push(service.id);
        });
    }

    async deleteService(service) {
        if ((this.state = States.DAY_CLOSED)) {
            throw DAY_CLOSED_ERROR;
        }

        return this.repository.dayServiceDelete(this.id, service.id).then(() => {
            this.services = this.services.filter(s => s !== service.id);
        });
    }

    toString() {
        return this.day;
    }
}
