export function queueRead(repository) {
    const today = new Date();
    today.setHours(0, 0, 0, 0);

    const clients = {};
    const operators = {};

    const clientsPromise = repository.queueClientsQuery(today)
        .then(clientRows => {
            clientRows.rows.forEach(clientRow => {
                clientRow.window = clientRow.window_;
                clientRow.registerTime = clientRow.reg_dt;
                clients[clientRow.client] = clientRow;
            });
        });
    const operatorsPromise = repository.queueOperatorsQuery(today)
        .then(operatorRows => {
            operatorRows.rows.forEach(operatorRow => {
                operatorRow.window = operatorRow.window_;
                operators[operatorRow.operator] = operatorRow;
            });
            return repository.queueOperatorsServicesQuery(today);
        })
        .then((operatorsServicesRows) => {
            for (const row of operatorsServicesRows.rows) {
                const operator = operators[row.operator];
                if (!operator) {
                    break;
                }
                if (!operator.services) {
                    operator.services = [];
                }
                operator.services.push(row.service);
            }
        });

    return Promise.all([clientsPromise, operatorsPromise])
        .then(() => {
            for (const operator of Object.values(operators).filter(o => o.client)) {
                operator.client = clients[operator.client];
            }
            return {operators, clients};
        });
}